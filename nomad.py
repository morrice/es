#!/usr/bin/python3
"""Allows the processing of nomad logs. Without all the shit.

- actually orders log entries based on timestamp ✓
- not limited by 500 entries (or whatever you manually set in the kibana UI) ✓
- runs in seconds as opposed to decades ✓
- uses kerberos auth and does not need 2fa ✓
- can be grepped ✓
- no need to click on stuff ✓
- nuff said ✓✓✓✓✓✓
"""

import argparse
from datetime import datetime, timedelta, timezone
import re
import sys

import pytz
from opensearchpy import OpenSearch, RequestsHttpConnection
from requests_kerberos import HTTPKerberosAuth, OPTIONAL

def parse_args():
    """ define command line arguments"""
    aparser = argparse.ArgumentParser(
        description='Get logs for a nomad tag')
    aparser.add_argument('--task',
                         help='nomad task to search for',
                         action='store',
                         dest='task')
    aparser.add_argument('--days',
                         help='days back to search for',
                         action='store',
                         type=int,
                         dest='days')
    aparser.add_argument('--hours',
                         help='hours back to search for',
                         action='store',
                         type=int,
                         dest='hours')
    aparser.add_argument('--minutes',
                         help='minutes back to search for',
                         action='store',
                         type=int,
                         dest='minutes')
    aparser.add_argument('--printhost',
                         help='print host that the job(s) ran on (default: false)',
                         action='store_true',
                         default=False,
                         dest='printhost')
    aparser.add_argument('--printtask',
                         help='print task names (default: false)',
                         action='store_true',
                         default=False,
                         dest='printtask')
    return aparser.parse_args()

def setup_query(task, timemeasure, timevalue):
    """setup the query from the template"""
    endtime = datetime.now(timezone.utc)
    if 'days' in timemeasure:
        starttime = endtime-timedelta(days=timevalue)
    elif 'hours' in timemeasure:
        starttime = endtime-timedelta(hours=timevalue)
    else:
        starttime = endtime-timedelta(minutes=timevalue)
    stime = starttime.strftime("%Y-%m-%dT%H:%M:%S")
    etime = endtime.strftime("%Y-%m-%dT%H:%M:%S")
    query = {'version': True, 'size': 10000, '_source': ['metadata.timestamp', 'data.tag', 'data.message', 'metadata.host'], 'query': {'bool': {'must': [{'query_string': {'query': '*', 'analyze_wildcard': True, 'default_field': '*'}}, {
        'bool': {'minimum_should_match': 1, 'should': [{'match_phrase': {'data.NOMAD_TASK_NAME': task}}]}}, {'range': {'metadata.timestamp': {'gte': stime, 'lte': etime, 'format': "yyyy-MM-dd'T'HH:mm:ss"}}}]}}}
    if task is None:
        del query['query']['bool']['must'][1]
    return query

def main():
    """main entry point"""

    args = parse_args()

    try:
        client = OpenSearch(
            ['https://os-linux.cern.ch/os'],
            use_ssl=True,
            verify_certs=True,
            connection_class=RequestsHttpConnection,
            http_auth=HTTPKerberosAuth(mutual_authentication=OPTIONAL)
        )
    except AuthenticationException:
        print("Failed to auth, try kinit?")
        sys.exit(1)

    if args.days is None and args.hours is None and args.minutes is None:
        timemeasure = 'days'
        timevalue = 1 
    elif args.days is not None and args.hours is not None:
        print("both days and hours passed, ignoring hours")
        timemeasure = 'days'
        timevalue = args.days
    elif args.hours is not None and args.minutes is not None:
        print("both hours and minutes passed, ignoring minutes")
        timemeasure = 'hours'
        timevalue = args.hours
    elif args.days is not None:
        timemeasure = 'days'
        timevalue = args.days
    elif args.hours is not None:
        timemeasure = 'hours'
        timevalue = args.hours
    elif args.minutes is not None:
        timemeasure = 'minutes'
        timevalue = args.minutes
    if args.task is None:
      print(f"Running query from the last {timevalue} {timemeasure}(s) (no tasks filtered)")
    else:
      print(f"Running query from the last {timevalue} {timemeasure}(s) (filtering for task: {args.task})")
    query = setup_query(args.task, timemeasure, timevalue)
    indexname = 'monit_private_lxsoft_logs_nomad*'
    iterator = client.search(body=query, index=indexname)

    records = []
    for hit in iterator['hits']['hits']:
        formatted = datetime.fromtimestamp(hit['_source']['metadata']['timestamp'] / 1000)
        utc_timezone = pytz.timezone('UTC')
        local_timezone = pytz.timezone('Europe/Zurich')
        utc_dt = utc_timezone.localize(formatted)
        local_dt = utc_dt.astimezone(local_timezone)
        # Remove bash colours from messing up output
        msg = re.sub(r'\[[0-1]\;\d\d', '', hit['_source']['data']['message'])
        records.append([local_dt.strftime('%Y%m%d %H:%M:%S'), hit['_source']['metadata']['host'], hit['_source']['data']['tag'], msg.replace('\r', ' ').strip()])
    records_sorted = sorted(records,key=lambda x:datetime.strptime(x[0],"%Y%m%d %H:%M:%S"))
    if len(records_sorted) != 0:
        fields = [ 0 ]
        if args.printhost:
            fields.append(1)
        if args.printtask:
            fields.append(2)
        for rec in records_sorted:
            msglen = len(rec[3])
            for f in fields:
                print(rec[f], end=' | ')
            print(rec[3])
    else:
        print("Nothing to see here!")

if __name__ == '__main__':
    sys.exit(main())
